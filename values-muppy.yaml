# This is the value.yaml file that will be used by muppy when it exists
# Muppy uses jinja template with special delimiters to update this file with dashboard values
mpy-metapackage:
  package_release:
    name: "@{ obj.name }@"
    key: "@{ obj.key }@"
    app_name: "@{ profile_obj.k8s_app_id.clug or obj.k8s_app_id.slug }@"
    app_version: "@{ profile_obj.app_version or package_obj.package_version }@"
    main_fqdn: "@{ obj.fqdn_hostname }@"

  # Cluster setup
  #default_storage_class: csi-cinder-high-speed
  #default_storage_class: microk8s-hostpath 
  @%- if k8s_cluster_obj.default_storage_class %@
  default_storage_class: "@{ k8s_cluster_obj.default_storage_class or '' }@" 
  @%- else %@
  default_storage_class: 
  @%- endif %@
  # Starting from v28.x.x, Traefik Helm Package deploy Traefik 3. before Traefik2
  traefik_major_version: 3
  
  tls:
    # ACME / Let's Encrypt
    acme:
      acme_server_url: "@{ k8s_cluster_obj.lets_encrypt_acme_effective_server_url }@"
      acme_registration_email: "@{ k8s_cluster_obj.lets_encrypt_acme_email }@"
    use_dns_challenge: @{ obj.use_dns_challenge }@
    dns_domain:
      dns_provider: "@{ obj.dns_domain_id.dns_provider }@"
  @%- if  obj.dns_domain_id.dns_provider == 'ovh' %@
      ovh_webhook_secret_data: 
        @{ obj.dns_domain_id.ovh_webhook_secret_data | indent(width=8, first=False) }@
  @%- endif %@
      # certmanager_webhook_ovh_groupName: 'pack8s-certmanager-webhook-ovh'
  @%- if  obj.dns_domain_id.provider == 'cloudflare' %@
      cloudflare_token_password: "@{ obj.dns_domain_id.cloudflare_token_id.password }@"
  @%- endif %@

  # Main part Docker Image
  image_registry_secret: "@{ profile_obj.docker_registry_secret_id.k8s_docker_config_json or '' }@"
  image_pull_policy: Always
  docker_image: "@{ profile_obj.docker_image or '' }@"

  # Secret Env File selection
  # TODO: Use a secret manager connected to Muppy
  # You can use a vault object of type:'Environment File' to store secrets and config env vars.
  # You must name your vault object as "{app.code}-env-{qualifier_short_alias}"
  #   eg. myapp-env-test, xayoni-env.prod, muppy-env-staging, ....
  @%- set envfile_secret_vault_code = "%s-env-%s" % (profile_obj.k8s_app_id.code, obj.qualifier_id.short_alias,) %@
  @%- set envfile_secret_vault_hash = vault_model.get(envfile_secret_vault_code).env_file_sha256 or 'n/a' %@
  # Here is content of vault objet with code '@{ envfile_secret_vault_code }@'
  secrets_env_file: 
  @%- if profile_obj %@
    @{ vault_model.get(envfile_secret_vault_code).get_env_vars_json(b64_encode=True) | to_yaml(offset=4) }@
  @%- endif %@

  #
  # Common ENV VARs
  # These ENV Vars are injected in all parts
  # Per part ENV Var can be defined in 
  #
  common_env_vars:
    - name: APP_PRIMARY_URL
      value: "https://@{ obj.fqdn_hostname }@"
    - name: APP_PRIMARY_FQDN
      value: "@{ obj.fqdn_hostname }@"
    - name: MPY_ENV_CHANGE_TRIGGER
      value: "@{ envfile_secret_vault_hash }@"
    - name: PGHOST
      value: "@{ obj.dash_pg_cluster_id.host_id.host_ip_id.ip_address }@"
    - name: PGPORT
      value: "@{ obj.dash_pg_cluster_id.port }@"
    - name: PGUSER
      value: "@{ obj.dash_pg_user or '' }@"
    - name: PGPASSWORD
      value: "@{ obj.dash_pg_password or '' }@"
    - name: PGDATABASE
      value: "@{ obj.dash_pg_database or '' }@"
    - name: DATABASE_URL
      value: "postgresql://@{obj.dash_pg_user}@:@{obj.dash_pg_password}@@@{obj.dash_pg_cluster_id.host_id.host_ip_id.ip_address}@:@{obj.dash_pg_cluster_id.port}@/@{obj.dash_pg_database}@"

    # PGFILTER is not used with k8S
    #- name: PGFILTER
    #  value: ""

  debug_env_vars:
    - name: PASSWORD
      value: "@{ obj.debug_credential_id.password or ''}@"

  #
  # Dashboards
  #
  # Example for cronjob
  #dashboards:
  #  demo-cron:
  #    suspend: true
  #    command: ["sleep"]
  #    args: ["7"]
  #    schedule: "*/1 * * * *"
  #    resources:
  #      requests:
  #        cpu: 400m
  #        memory: 400M
  #      limits:
  #        cpu: 800m
  #        memory: 800M
  dashboards:
    @{ obj.get_dashboards_json() | to_yaml(offset=4) }@



  # IP Whitelisting middleware config
  # To activate, uncomment then update allowed CIDRs list ; one per line. IP are allowed
  # Warning:
  #   On public Clusters, to restrict access to private network, you must set 
  #   allowed list to host Public IP.
  #   Example:
  #   ---
  #   ipwhitelist:
  #     run:
  #     - 137.74.201.210         # host public IP
  #   ---
  #
  #ipwhitelist:
  #  run:
  #   x.x.x.y
  #  debug
  #   x.y.z.e/24
  #
  ipwhitelist:
    enable_run: @{ obj.enable_ipwhitelist_run }@
    enable_debug: @{ obj.enable_ipwhitelist_debug }@
    run:
      @{ obj.allowed_cidr_dyna_range_run_id.cidr_ids.mapped('cidr') | to_yaml(offset=6) }@
    debug:
      @{ obj.allowed_cidr_dyna_range_debug_id.cidr_ids.mapped('cidr') | to_yaml(offset=6) }@

  # basicAuth middleware
  # Allow to define a user / password. 
  # Avoid to use it. Or only brifly
  # vault_model.get(values.traefik_dashboard_auth_vault_name).get_htpasswd() 
  basicAuth:
    enable_run: @{ obj.enable_http_basic_auth_run }@
    enable_debug: @{ obj.enable_http_basic_auth_debug }@
    @%- if obj.enable_http_basic_auth_run or obj.enable_http_basic_auth_run %@
    users: |
      @{ obj.http_basic_auth_vault_id.get_htpasswd() }@
    @%- endif %@

  # TocToc
  # See toctoc documentation
  toctoc:
    enable_run: @{ obj.enable_toctoc_run }@
    enable_debug: @{ obj.enable_toctoc_debug }@
    agent_url: "http://@{ obj.toctoc_agent_service_id.intra_cluster_fqdn }@@{ obj.toctoc_agent_path }@"
    #agent_url: full (internal) url to toctoc agent
    #agent_url: http://{{toctoc-service-name}}.{{namespace}}.svc.cluster.local:8069/toctoc/fwdauth  # intra cluster different namespace
    #agent_url: http://toctoc-b63ce14d-gui-service/toctoc/fwdauth    # intra cluster same namespace

# Here is where you configure you App
# You should use a distinct file. Convention is to name it 'mpy_meta_config.yaml'.
# If you need you can inject all app config here.
  mpy_meta_config:
    parts:
      dashboard:  # This is a part
        type: http
        routes:
        - name: auth
          pathPrefix: "PathPrefix(`/api/v1/login`) || PathPrefix(`/api/v1/csrftoken/login`) || PathPrefix(`/api/v1/me`)"
          service_name: kdash-kubernetes-dashboard-auth
          port_name: auth
          middlewares:
            ipwhitelist: true
          priority: 10
        - name: api
          pathPrefix: "PathPrefix(`/api`) || PathPrefix(`/metrics`)"
          service_name: kdash-kubernetes-dashboard-api
          port_name: api
          middlewares:
            ipwhitelist: true
          priority: 20
        - name: webclient
          pathPrefix: "PathPrefix(`/`)"
          service_name: kdash-kubernetes-dashboard-web
          port_name: web
          middlewares:
            ipwhitelist: true
          priority: 30

        debug_mode_available: null  # No Pod = No Debug Mode 
