# Muppy k8s Dashboard ingress (for dashboard v3)

This package complements the Official Kubernetes Dashboard to:
  - expose it via a Traefik Ingress Route
  - help token management

The branches matches the kubernetes-dashboard.

Starting from v3, kubernetes-dashboard must be installed via helm.

Starting from v3, package versionning has changed to match kubernetes-dashboard versions.


Check Muppy documentation for Infrastructure Package installation. [Temporary link](https://cmorisse.gitlab.io/mpy13docs/muppy_k8s/installation/infra_packages_installation/)

# Dashboard Ingress (this package) Installation

You must have install kubernetes-dashboard with instructions above and noted his instance name (kdash by default).

Create a Package Release with:
- Package Profile: `Kubernetes Dashboard Ingress:7.5.0`
- Namespace: kubernetes-dashboard
- Qualifier: Infrastructure
- Host: Choose an meaningless name (avoid "Kubernetes Dashboard"). By default muppy will propose 'hoob'
- Network:
	- DNS Domain: Select domain used to publish
	- Hostname Generator: `hostname.clustername.domain.ext - PROD``
	- Loadbalancer: Select the Cluster LoadBalancer
- Values:
  - Adjust 'kubernetes-dashboard-helm-instance-name' if you didn't use the `kdash` default.

Click on the [Install] button.

# Usage 

Once installed, go the Dashboard tab of your K8s Cluster.

 - click on the Extract Token button
 - Click on 'Copy Text' to save the Token
 - Click on the 'Open Dashboard' button
 - Paste the Token to login

## Manuel token generation

```bash
# On a shell on cluister
microk8s kubectl -n kubernetes-dashboard create token --duration=60m mpy-dashboard-admin
```

# Traefik Deprecation warning

 * `traefik.containo.us/v1alpha1` must be replaced by `traefik.io/v1alpha1` starting Traefik 2.10
 * `traefik.containo.us/v1alpha1` and `traefik.io/v1alpha1` must be replaced by `traefik.io/v1` starting Traefik 3


# Build this package

```bash
# in chart folder

# This will rebuild dependencies
rm -rf charts
rm -rf Chart.lock
helm dependency build .

helm package --dependency-update . --destination=pkg

# refresh
helm dependency update .


# 
helm -n kubernetes-dashboard list
helm -n kubernetes-dashboard delete kdash

helm -n kubernetes-dashboard install --debug --dry-run kdash-ingress . -f values.yaml 
helm -n kubernetes-dashboard install --debug --dry-run kdash-ingress . -f values.yaml > helm-debug.txt
```

# To publish the Helm Chart to Gitlab Registry

Adjust and commit all changes before.

```bash
cd utils
export HELM_CHART_NAME=$(basename $(helm package --dependency-update .. | awk '{print $NF}'))  && echo "chart name=${HELM_CHART_NAME}" 
export GITLAB_REGISTRY_USERNAME=cmorisse
export GITLAB_REGISTRY_TOKEN=$REPO_GIT_TOKEN
export GITLAB_PROJECT_ID=56454873
# To upload the package to gitlab registry
curl --request POST \
    --form "chart=@$HELM_CHART_NAME" \
    --user $GITLAB_REGISTRY_USERNAME:$GITLAB_REGISTRY_TOKEN \
    https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/packages/helm/api/stable/charts
rm $HELM_CHART_NAME
```

# To install the package from Gitlab Package Repository (For Dev)

helm repo add mpy-kubernetes-dashboard-ingress-charts https://gitlab.com/api/v4/projects/56454873/packages/helm/stable
helm repo update mpy-kubernetes-dashboard-ingress-charts

helm install kdash mpy-kubernetes-dashboard-ingress-charts/mpy-kubernetes-dashboard-ingress
```

